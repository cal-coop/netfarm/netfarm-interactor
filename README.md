# a Netfarm interactor

![an image of the Netfarm interactor](interactor.png)
![an image of the instance maker](make-instance-graphically.png)

This is a graphical client for the 
[Netfarm](https://gitlab.com/cal-coop/netfarm/netfarm) distributed object 
system, using the *Common Lisp Interface Manager* protocol and the 
[proposed "presentation" protocol](https://gitlab.com/cal-coop/netfarm/netfarm/issues/2)
which is derived from the presentation protocol and declarative layout 
definitions CLIM provides.

To start the interactor, evaluate `(netfarm-interactor:interactor)`. To view an 
object, evaluate `(netfarm-interactor:inspect-object <object>)`. To make an
instance of a Netfarm class, evaluate
`(netfarm-interactor:make-instance-graphically <class>)`.

## Commands

- `Add Connection (URI) <string>` Add a connection (or knowledge of a URI) to 
  the interactor.
- `Find Object (object hash) <hash>` View the object whose hash is `<hash>`.
- `Generate Keys` Generate keys and view the public key object.
- `List Connections` List the current connections and known URIs.
- `Inspect Object (object) <object>` View the object provided. This is performed
  when you click on an object, so you shouldn't have to perform it by name.
- `Inspect Schema of Object` View the schema of the current object.
- `Redisplay` Redisplay the object. This will be removed when I figure out how
  to re-wrap content when the window is resized with CLIM.
