(in-package :netfarm-interactor)

(define-netfarm-interactor-app-command (com-list-connections :name t)
    ()
  (let ((client (client clim:*application-frame*)))
    (format *query-io* "Currently connected clients: ~{~a~^, ~}~%Known URIs: ~{~a~^, ~}"
            (mapcar (alexandria:compose #'decentralise-connection:connection-uri
                                        #'decentralise-client:client-connection)
                    (decentralise-client:client-subclients client))
            (decentralise-kademlia:kademlia-client-known-uris client))))
            
(define-netfarm-interactor-app-command (com-add-connection :name t)
    ((uri string :prompt "URI"))
  (handler-case
      (decentralise-kademlia:add-connection-from-uri
       (client clim:*application-frame*) uri)
    (error (e)
      (write-error e))))
   
