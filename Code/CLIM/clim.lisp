(in-package :netfarm-interactor)

(defclass graphical-view (clim:view)
  ())
(defclass resizing-application-pane (clim:application-pane)
  ())

(defun write-error (error &optional (stream *query-io*))
  (clim:with-text-face (stream :italic)
    (princ error stream)))

(clim:define-application-frame netfarm-interactor-app ()
  ((client :initarg :client
           :initform (make-instance 'netfarm-client:client :bootstrap-uris '())
           :accessor client)
   (keys :initarg :keys
         :accessor keys)
   (history :initform '() :accessor history)
   (history-pane :accessor history-pane)
   (netfarm-presentation :accessor netfarm-presentation)
   (object :initarg :object
           :initform *introduction*
           :accessor interactor-object)
   (presentation-pane :accessor presentation-pane))
  (:panes
   (history :application
            :display-function 'display-history
            :min-width 250
            :max-width 250)
   (interactor :interactor :max-height 150)
   (presentation resizing-application-pane
                 :display-function 'display-presentation
		 :end-of-line-action #+mcclim :wrap* #-mcclim :wrap
                 :default-view (make-instance 'graphical-view)
		 :min-width 400)
   (pointer-documentation :pointer-documentation :scroll-bars nil))
  (:layouts
   (default
    (clim:vertically ()
      (clim:horizontally ()
        (setf (presentation-pane clim:*application-frame*)
              presentation)
        (setf (history-pane clim:*application-frame*)
              history))
      interactor
      pointer-documentation))))

(defmethod initialize-instance :after ((app netfarm-interactor-app) &key)
  (push (interactor-object app) (history app))
  (update-presentation app))

(define-netfarm-interactor-app-command
    (com-find-object :name t :menu t)
    ((hash string :prompt "Object hash"))
  (handler-case (netfarm-client:find-object (client clim:*application-frame*)
                                            hash
                                            :timeout 20)
    (:no-error (object)
      (setf (interactor-object clim:*application-frame*)
            object)
      (pushnew object (history clim:*application-frame*))
      (update-presentation clim:*application-frame*))
    (error (e)
      (write-error e))))

(define-netfarm-interactor-app-command
    (com-inspect-object :name t)
    ((object netfarm:object :prompt t))
  (setf (interactor-object clim:*application-frame*) object)
  (pushnew object (history clim:*application-frame*))
  (update-presentation clim:*application-frame*))

(define-netfarm-interactor-app-command
    (com-inspect-schema-of-object :name t :menu t)
    ()
  (let ((schema (netfarm:class->schema
		 (class-of (interactor-object clim:*application-frame*)))))
    (setf (interactor-object clim:*application-frame*) schema)
    (pushnew schema (history clim:*application-frame*))
    (update-presentation clim:*application-frame*)))

(define-netfarm-interactor-app-command
    (com-generate-keys :name t)
    ()
  (let* ((keys (netfarm:generate-keys))
         (object (netfarm:keys->object keys)))
    (setf (keys clim:*application-frame*)
          keys
          (interactor-object clim:*application-frame*)
          object)
    (pushnew object (history clim:*application-frame*))
    (update-presentation clim:*application-frame*)))

(define-netfarm-interactor-app-command (com-redisplay :name t)
    ()
  (setf (clim:pane-needs-redisplay (presentation-pane clim:*application-frame*))
        t
        (clim:pane-needs-redisplay (history-pane clim:*application-frame*))
        t))

(defun update-presentation (app)
  (let* ((netfarm-scripts:*default-presentation-script*
           *default-presentation-script*)
         (presentation (netfarm-scripts:compute-presentation
                        (interactor-object app))))
    (setf (netfarm-presentation app) presentation)
    (when (slot-boundp app 'presentation-pane)
      (setf (clim:pane-needs-redisplay (presentation-pane app)) t))))

(defun interactor ()
  (clim:run-frame-top-level
   (clim:make-application-frame 'netfarm-interactor-app)))
(defun inspect-object (object)
  (check-type object netfarm:object)
  (clim:run-frame-top-level
   (clim:make-application-frame 'netfarm-interactor-app
                                :object object)))
