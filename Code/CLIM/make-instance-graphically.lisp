(in-package :netfarm-interactor)

(clim:define-application-frame instance-maker-app ()
  ((class :initarg :netfarm-class :reader instance-maker-class)
   (pane-table :initform (make-hash-table :test 'equal)
               :accessor instance-maker-pane-table))
  (:panes (description :label :label (format nil "Creating an instance of ~s"
                                             (instance-maker-class clim:*application-frame*)))
          (slots (make-slots-from-app))
          (done-button :push-button
                       :label "Create instance"
                       :max-height 32
                       :activate-callback 'finish-making-instance))
  (:layouts
   (default (clim:vertically ()
              description
              slots
              done-button))))

(defun make-slots-from-app ()
  (let ((slot-table (instance-maker-pane-table clim:*application-frame*))
        (class (instance-maker-class clim:*application-frame*))
        (rows '()))
    (netfarm:map-slots (lambda (name definition)
                         (declare (ignore definition))
                         (let ((label (clim:make-pane 'clim:label-pane
                                                      :label name
                                                      :max-width 100
                                                      :min-width 100))
                               (entry-box (clim:make-pane 'clim:text-field-pane
                                                          :min-width 300))
                               (unboundp-box (clim:make-pane 'clim:check-box-pane
                                                             :choices '("unbound?")
                                                             :max-width 100)))
                           (setf (gethash name slot-table)
                                 (list entry-box unboundp-box))
                           (push (list name label entry-box unboundp-box)
                                 rows)))
                       class)
    (setf rows (sort rows #'string< :key #'car))
    (clim:make-pane 'clim:table-pane
                    :contents (mapcar #'cdr rows))))
      
(defun finish-making-instance (button)
  (declare (ignore button))
  (let* ((instance (make-instance (instance-maker-class clim:*application-frame*))))
    (maphash (lambda (name boxes)
               (destructuring-bind (entry unboundp)
                   boxes
                 (unless (clim:gadget-value unboundp)
                   (setf (netfarm:object-value instance name)
                         (with-input-from-string (s (clim:gadget-value entry))
                           (netfarm:parse s))))))
             (instance-maker-pane-table clim:*application-frame*))
    (setf *instance* instance)
    (clim:frame-exit clim:*application-frame*)))

(defvar *instance*)
(defgeneric make-instance-graphically (netfarm-class)
  (:method ((class netfarm:netfarm-class))
    (let ((*instance* nil))
      (clim:run-frame-top-level
       (clim:make-application-frame 'instance-maker-app
				    :netfarm-class class))
      *instance*)))
