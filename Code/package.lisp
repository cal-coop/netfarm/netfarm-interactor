(defpackage :netfarm-interactor
  (:use :cl)
  (:export #:interactor #:interactor-object
           #:netfarm-interactor-app
           #:inspect-object
           #:make-instance-graphically #:update-presentation))
