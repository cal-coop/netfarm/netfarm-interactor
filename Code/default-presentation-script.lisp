(in-package :netfarm-interactor)

(defvar *default-presentation-script*
  (netfarm-slacker:compile-program-from-file
   (asdf:system-relative-pathname :netfarm-interactor "default-presentation.scm")))
