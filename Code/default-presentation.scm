(define (present object)
  ((lambda (s)
     (list 'vertically
           (header object s)
           (body object s)))
   (schema object)))

(define (header object s)
  (list 'sequentially object ": an instance of " s))

(define (body object s)
  (list 'vertically
        '(header "Slots")
        (present-slots object
                       (object-value s 'slots)
                       object-value
                       object-bound?)
        '(header "Computed slots")
        (present-slots object
                       (object-value s 'computed-slots)
                       object-computed-values
                       (lambda (o n) #t))))

(define (present-slots object slot-definitions reader test)
  (cons 'tabling
        (cons '("Name" "Value")
              (%present-slots object slot-definitions reader test))))

(define (%present-slots object slot-definitions reader test)
  (if (null? slot-definitions)
      '()
      ((lambda (name)
         (cons (list name
                     (if (test object name)
                         (list 'escaping (reader object name))
                         (list 'italic "unbound")))
               (%present-slots object (cdr slot-definitions) reader test)))
       (car (car slot-definitions)))))
                  
