(asdf:defsystem :netfarm-interactor
  :depends-on (:netfarm-client :netfarm-slacker :mcclim)
  :author "Cooperative of Applied Language"
  :version "0.1.0"
  :license "Cooperative Software License v1+"
  :components ((:file "package")
               (:file "introduction")
	       (:file "default-presentation-script")
               (:module "CLIM"
                :components ((:file "clim")
                             (:file "client")
			     (:file "make-instance-graphically")
                             (:file "translate-presentation")))))
