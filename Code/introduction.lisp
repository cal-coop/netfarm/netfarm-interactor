(in-package :netfarm-interactor)

(netfarm-scripts:define-script *identity-presentation-script* ("presentation")
  (:method "present" 0)
  self (get-value 0) object-value return)
  
(defclass identity-presentation ()
  ((presentation :initarg :presentation :reader identity-presentation-presentation)
   (name :initarg :name :reader identity-presentation-name))
  (:scripts *identity-presentation-script*)
  (:metaclass netfarm:netfarm-class))

(defmethod print-object ((presentation identity-presentation) stream)
  (if *print-readably*
      (call-next-method)
      (format stream "«~a»" (identity-presentation-name presentation))))

(defvar *test-object*
  (make-instance 'identity-presentation
                 :presentation `("vertically"
                                 ("header" "Here is a test object.")
                                 "Here are some test values:"
                                 ("tabling"
                                  ("Type" "Value")
                                  ("Integer" 42)
                                  ("Boolean" :true)
                                  ("The other boolean" :false)
                                  ("String" "This is not my beautiful type!"))
                                 ("sequentially"
                                  "Here is a little program:"
                                  ("code" "#1=(defun quine () (let ((*print-circle* t)) (print '#1#)))")))
                 :name "A test object"))

(defvar *introduction*
  (make-instance 'identity-presentation
                 :presentation `("vertically"
                                 ("header" "Welcome to Netfarm")
                                 "Netfarm is our attempt at creating yet another distributed internet thingamabob. It uses a distributed hash table, digital signatures and a collection of relatively simple text formats to allow objects to be stored without possibly being tampered with, regardless of implementation or application details."
                                 ("horizontally" "Here is a test object you can click on to view:" ,*test-object*))
                 :name "Introduction"))
